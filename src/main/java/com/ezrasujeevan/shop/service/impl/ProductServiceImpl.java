package com.ezrasujeevan.shop.service.impl;

import com.ezrasujeevan.shop.model.ProductModel;
import com.ezrasujeevan.shop.repository.ProductRepository;
import com.ezrasujeevan.shop.service.ProductService;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final double handleRate = 1.3;
    private final double discountRate = 0.9;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductModel> getAllProducts() {
        return productRepository.findAll();

    }

    @Override
    public String getPriceTable(long id) throws JSONException {
        ProductModel pm = productRepository.getOne(id);
        double unitPrice = pm.getPrice() / pm.getCartonQuantity();
        JSONArray jsonArray = new JSONArray();

        for (int i = 1; i <= 50; i++) {
            double price = 0;
            if (i % pm.getCartonQuantity() == 0) {
                price = i * unitPrice;
            } else {
                int totalCarton = (i / pm.getCartonQuantity());
                if (totalCarton > 2) {
                    price = (unitPrice * totalCarton * discountRate) + (unitPrice * handleRate * (i % pm.getCartonQuantity()));
                } else {
                    price = (unitPrice * totalCarton) + (unitPrice * handleRate * (i % pm.getCartonQuantity()));
                }
            }
            JSONObject jsonObject = new JSONObject().put("total_price", price).put("count", i);

            jsonArray.put(jsonObject);
        }

        return jsonArray.toString();
    }

    @Override
    public String calculateProduct(long id, int cartonQuantity, int individualQuantity) throws JSONException {
        ProductModel pm = productRepository.getOne(id);

        double unitPrice = pm.getPrice() / pm.getCartonQuantity();
        int individualCarton = individualQuantity / pm.getCartonQuantity();
        int individualRemainPieces = individualQuantity % pm.getCartonQuantity();
        int totalCartons = individualCarton + cartonQuantity;
        double individualCartonPrice = totalCartons * pm.getPrice();
        double individualRemainPiecesPrice = unitPrice * individualRemainPieces;

        if (totalCartons > 2) {
            individualCartonPrice = individualCartonPrice * discountRate;
        }
        double totalPrice = individualCartonPrice + individualRemainPiecesPrice;
        JSONObject jsonObject = new JSONObject().put("total_cartons", totalCartons).put("individual_pieces", individualRemainPieces).put("total_price", totalPrice);

        return jsonObject.toString();
    }
}
