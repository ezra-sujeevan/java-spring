package com.ezrasujeevan.shop.service;

import com.ezrasujeevan.shop.model.ProductModel;
import org.json.JSONException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<ProductModel> getAllProducts();
    String getPriceTable(long id) throws JSONException;
    String calculateProduct(long id , int cartonQuantity,int individuallyQuantity) throws JSONException;
}
