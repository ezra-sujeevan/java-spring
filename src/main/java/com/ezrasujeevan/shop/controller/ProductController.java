package com.ezrasujeevan.shop.controller;

import com.ezrasujeevan.shop.model.ProductModel;
import com.ezrasujeevan.shop.service.ProductService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin()
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public List<ProductModel> getProducts()
    {
        return this.productService.getAllProducts();
    }


    @GetMapping("/table/{id}")
    public String getPriceTable(@PathVariable("id") long id){
        try {
            return this.productService.getPriceTable(id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    @GetMapping("/calculator/{id}")
    public String getCalculator(@PathVariable("id") long id,@RequestParam(name="carton_qty") int cartonQuantity,@RequestParam(name="individual_qty") int individualQuantity)
    {
        try {
            return this.productService.calculateProduct(id,cartonQuantity,individualQuantity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
