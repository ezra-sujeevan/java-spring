package com.ezrasujeevan.shop.model;



import javax.persistence.*;

@Entity
@Table(name = "product")
public class ProductModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @Column(name = "carton_qty")
    private int cartonQuantity;

    public ProductModel() {
    }

    public ProductModel(long id, String name, double price, int cartonQuantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.cartonQuantity = cartonQuantity;
    }

    public ProductModel(String name, double price, int cartonQuantity) {
        this.name = name;
        this.price = price;
        this.cartonQuantity = cartonQuantity;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCartonQuantity() {
        return cartonQuantity;
    }

    public void setCartonQuantity(int cartonQuntity) {
        this.cartonQuantity = cartonQuntity;
    }


}
