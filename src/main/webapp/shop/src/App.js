import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import NavigationBar from './componants/NavigationBar';
import Welcome from './componants/Welcome';
import Footer from './componants/Footer';
import CalculatePrice from './componants/CalculatePrice';
import PriceTable from './componants/PriceTable';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './App.css';

function App() {
  const marginTop = {
    marginTop: "20px"
  };

  return (
    <div className="App">
      <Router>
        <NavigationBar />
        <Container>
          <Row>
            <Col lg={12} style={marginTop}>
              <Switch>
                <Route path="/" exact component={Welcome}/>
                <Route path="/table" exact component={PriceTable}/>
                <Route path="/calculator" exact component={CalculatePrice}/>
              </Switch>
            </Col>
          </Row>
        </Container>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
