import React, { useEffect, useState } from 'react';
import { Card, Form, InputGroup, Button } from 'react-bootstrap'



function CalculatePrice() {

    const [products, setProducts] = useState([])
    const [values, setValue] = useState({ cartons: 0, units: 0, item: 0, price: 0, readOnly: true })

    useEffect(() => {
        fetchProducts()
    }, [])

    const fetchProducts = async () => {
        const responce = await fetch(`http://localhost:8080/`)
        const data = await responce.json();
        console.log(data)
        setProducts(data)

    }

    const getPrice = async () => {
        const response = await fetch(`http://localhost:8080/calculator/${values.item}?carton_qty=${values.cartons}&individual_qty=${values.units}`)
        const data = await response.json();
        console.log(data)
        setValue(prevState => {
            return { ...prevState, price: data.total_price }
        })
        setValue(prevState => {
            return { ...prevState, readOnly: !prevState.readOnly }
        })


    }

    const calculatePrice = e => {
        e.preventDefault()
        if (values.item !== 0 && (values.cartons > 0 || values.units > 0)) {
            setValue(prevState => {
                return { ...prevState, readOnly: !prevState.readOnly }
            })
            getPrice()

        }
        else {
            alert("Please Select a Product and increase the unit or carton count")
        }

    }

    const valueChangeCarton = e => {
        const val = e.target.value
        setValue(prevState => {
            return { ...prevState, cartons: val }
        })

    }
    const valueChangeItem = e => {
        const val = e.target.value
        setValue(prevState => {
            return { ...prevState, item: val }
        })

    }
    const valueChangeUnit = e => {
        const val = e.target.value
        setValue(prevState => {
            return { ...prevState, units: val }
        })

    }


    return (
        <Card className="border border-dark bg-dark text-white text-left">
            <Card.Title> Calculate Price</Card.Title>
            <Card.Body>
                <Form onSubmit={calculatePrice}>
                    <Form.Group controlId="exampleForm.ControlSelect1" >
                        <Form.Label>Product</Form.Label>
                        <Form.Control as="select" className="bg-dark text-white" name="item" onChange={valueChangeItem}>
                            <option value="0">Please select a Product</option>
                            {
                                products.map(product =>
                                    (<option key={product.id} value={product.id}> {product.name} </option>)
                                )
                            }
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>No Of Cartons</Form.Label>
                        <Form.Control name="cartons" type="number" placeholder="5" min="0" className="bg-dark text-white" value={values.cartons} onChange={valueChangeCarton} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>No Of Items</Form.Label>
                        <Form.Control name="units" type="number" placeholder="20" min="0" className="bg-dark text-white" value={values.units} onChange={valueChangeUnit} />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Calculate
                    </Button>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Total Price</Form.Label>
                        <InputGroup className="mb-2">
                            <InputGroup.Prepend>
                                <InputGroup.Text>$</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control readOnly={values.readOnly} value={values.price} className="bg-dark text-white" />
                        </InputGroup>
                    </Form.Group>
                </Form>
            </Card.Body>
        </Card>
    );

}
export default CalculatePrice;