import React from 'react';
import {  Navbar, Container, Row ,Col} from 'react-bootstrap'

class Footer extends React.Component {
    render() {
        return (
            <Navbar fixed="bottom" bg="dark" variant="dark">
                <Container>
                    <Row>
                        <Col lg={12} className="text-muted text-center">
                        <div>Created by Ezra</div>
                        </Col>
                    </Row>
                </Container>
            </Navbar>
        );
    }
}
export default Footer;