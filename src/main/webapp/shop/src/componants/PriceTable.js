import React, { useEffect, useState } from 'react';
import { Card, Form, Table } from 'react-bootstrap'

function PriceTable() {

    const [products, setProducts] = useState([])
    const [select, setSelect] = useState({ item: 0 })
    const [priceTable, setPriceTable] = useState([])

    useEffect(() => {
        fetchProducts()
    }, [])

    useEffect(() => {
        getPriceTable()
    }, [select])

    const fetchProducts = async () => {
        console.log('fetch run')
        const response = await fetch(`http://localhost:8080/`)
        const data = await response.json();
        console.log(data)
        setProducts(data)

    }

    const getPriceTable = async () => {

        if (select.item !== 0) {
            const response = await fetch(`http://localhost:8080/table/${select.item}`)
            const data = await response.json();
            setPriceTable(data)
            console.log(data)
        }

    }

    const valueChangeItem = e => {
        const val = e.target.value
        setSelect({item: val})

    }
    
    

    return (
        <Card className="border border-dark bg-dark text-white">
            <Card.Title>Price Table</Card.Title>
            <Card.Body>
                <Form>
                    <Form.Group controlId="exampleForm.SelectCustom">
                        <Form.Label>Product</Form.Label>
                        <Form.Control as="select" className="bg-dark text-white" name="item" onChange={valueChangeItem}>
                            <option value="0">Please select a Product</option>
                            {
                                products.map(product =>
                                    (<option key={product.id} value={product.id}> {product.name} </option>)
                                )
                            }
                        </Form.Control>
                    </Form.Group>
                </Form>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>Quantity </th>
                            <th>Price</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            select.item === '0' ?
                                <tr>
                                    <td colSpan="2">Please Select A Product</td>
                                </tr> 
                                :
                                priceTable.map(price => (
                                    <tr>
                                        <td>{price.count}</td>
                                        <td>{price.total_price}</td>
                                    </tr>
                                ))
                        }



                    </tbody>
                </Table>

            </Card.Body>
        </Card>
    );

}
export default PriceTable;