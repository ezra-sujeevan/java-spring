import React from 'react';
import { Navbar, Nav } from 'react-bootstrap'
import logo from '../logo.svg';
import { Link } from 'react-router-dom'

class NavigationBar extends React.Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Link to="/"className="navbar-brand">
                <img alt="" src={logo} width="30" height="30" className="d-inline-block align-top" />{' '}    React Bootstrap
                </Link>
                <Navbar.Brand href="#home">
                    
                </Navbar.Brand>
                <Nav className="mr-auto">
                    <Link to="/table" className="nav-link">Price Table</Link>
                    <Link to="/calculator" className="nav-link">Calculate Price</Link>
                </Nav>
            </Navbar>
        );
    }
}
export default NavigationBar;
